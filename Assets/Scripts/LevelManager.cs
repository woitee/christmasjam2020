﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {
    public delegate void ButtonChangeListener(GameColor color);

    #region Singleton
    private static LevelManager _instance;
    public static LevelManager Instance { get { return _instance; } }
    private void Awake() {
        // Singleton-related functionality
        if (_instance != null && _instance != this) {
            Destroy(this.gameObject);
        } else {
            _instance = this;
        }
    }
    #endregion

    public bool ClickableBoxes = false;
    private List<ButtonChangeListener> listeners = new List<ButtonChangeListener>();

    private void Update() {
        if (Input.GetKeyDown(KeyCode.R)) {
            RestartLevel();
        }
    }
    
    public void AddButtonChangeListener(ButtonChangeListener listener) {
        listeners.Add(listener);
    }

    public void ButtonPressed(GameColor color) {
        foreach (var listener in listeners) {
            listener(color);
        }
    }

    public void ButtonUnpressed(GameColor color) {
        foreach (var listener in listeners) {
            listener(color);
        }
    }

    public void GoToNextLevel() {
        var scene = SceneManager.GetActiveScene();
        string nextSceneName = "dummy";
        if (scene.name.StartsWith("Level ")) {
            int currentLevelNumber = int.Parse(scene.name.Substring(6));

            if (currentLevelNumber < 7) {
                nextSceneName = "Level " + (currentLevelNumber + 1).ToString("D2");
            } else {
                nextSceneName = "Game End";
            }
        }

        GoToLevelByName(nextSceneName);
    }

    public void RestartLevel() {
        GoToLevelByName(SceneManager.GetActiveScene().name);
    }

    private void GoToLevelByName(string name) {
        SceneManager.LoadScene(name);
        SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene().name);
    }
}
