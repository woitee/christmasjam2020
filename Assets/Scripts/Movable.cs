﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Movable : MonoBehaviour {
    public struct MoveCheck {
        public bool canMove;
        public List<Movable> movablesInTheWay;

        public MoveCheck(bool success, List<Movable> movablesInTheWay) {
            this.canMove = success;
            this.movablesInTheWay = movablesInTheWay;
        }

        public static MoveCheck failure = new MoveCheck(false, new List<Movable>());

        public static MoveCheck CanMove(List<Movable> movablesInTheWay) {
            return new MoveCheck(true, movablesInTheWay);
        }
        public static MoveCheck CannotMove() {
            return failure;
        }
    }
    public float movementSpeed = 10f;

    protected bool isMoving = false;
    protected Vector3 movementTarget = Vector3.zero;

    private Rigidbody2D rigidBody = null;

    // Start is called before the first frame update
    protected void Start() {
        rigidBody = GetComponent<Rigidbody2D>();
    }

    void Update() {}
    protected void FixedUpdate() {
        UpdateMovement();
    }

    public void InitiateMovement(Vector2Int direction) {
        var moveCheck = CheckMovement(direction);
        if (!moveCheck.canMove) return;
        MoveStart(direction);

        movementTarget = transform.position;
        movementTarget.x += direction.x;
        movementTarget.y += direction.y;

        foreach (var movable in moveCheck.movablesInTheWay) {
            movable.InitiateMovement(direction);
        }

        isMoving = true;
    }

    protected virtual void MoveStart(Vector2Int direction) { }
    protected virtual void MoveEnd() { }

    public MoveCheck CheckMovement(Vector2Int direction) {
        var futureCollisions = Physics2D.BoxCastAll(transform.position, Vector2.one * 0.9f, 0.0f, direction, 0.9f)
                                        .Where(collision =>
                                            collision.collider.gameObject != gameObject
                                            && collision.collider.isTrigger == false
                                        )
                                        .ToList();

        var movablesInTheWay = new List<Movable>();
        // Check if the collisions can be moved
        foreach (var collision in futureCollisions) {
            var gameObject = collision.transform.gameObject;
            var movable = gameObject.GetComponent<Movable>();
            var isMovable = movable != null && movable.enabled == true;

            if (!isMovable || !movable.CheckMovement(direction).canMove) return MoveCheck.CannotMove();
            if (isMovable) movablesInTheWay.Add(movable);
            
        }

        return MoveCheck.CanMove(movablesInTheWay);
    }

    protected void UpdateMovement() {
        if (!isMoving) return;

        var currentTarget = Vector3.MoveTowards(transform.position, movementTarget, movementSpeed * Time.fixedDeltaTime);
        transform.position = currentTarget;

        if (currentTarget.ApproximatelyEquals(movementTarget)) {
            MoveEnd();
            isMoving = false;
        }
    }
}
