﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BoxBehavior : Movable {
    Stack<Vector2Int> performedMovements = new Stack<Vector2Int>();
    static bool performingBacktracking = false;

    private SpriteRenderer spriteRenderer;

    public Sprite defaultSprite;
    public Sprite redBox;
    public Sprite greenBox;
    public Sprite blueBox;

    private new void Start() {
        base.Start();

        spriteRenderer = GetComponent<SpriteRenderer>();
        performedMovements.Clear();
    }

    protected override void MoveStart(Vector2Int direction) {
        if (!performingBacktracking) {
            Debug.Log(string.Format("Pushing movement {0}", direction));
            performedMovements.Push(direction);
        }
    }

    protected override void MoveEnd() {
        Collider2D[] colliders = new Collider2D[10];
        var filter = new ContactFilter2D() { useTriggers = true };
        GetComponent<BoxCollider2D>().OverlapCollider(filter, colliders);
        var buttonBehaviors = colliders
            .Where(it => it != null)
            .Select(collider => collider.gameObject.GetComponent<ButtonBehavior>())
            .Where(it => it != null)
            .ToList();

        if (buttonBehaviors.Count == 0) {
            spriteRenderer.sprite = defaultSprite;
            return;
        }

        switch (buttonBehaviors[0].color) {
            case GameColor.RED:
                spriteRenderer.sprite = redBox;
                break;
            case GameColor.GREEN:
                spriteRenderer.sprite = greenBox;
                break;
            case GameColor.BLUE:
                spriteRenderer.sprite = blueBox;
                break;
        }
    }

    public void OnMouseUp() {
        if (performedMovements.Count == 0) return;

        var lastMove = performedMovements.Pop();
        var intendedMove = lastMove * -1;
        if (!CheckMovement(intendedMove).canMove) return;

        Debug.Log(string.Format("Popping movement {0}", lastMove));

        performingBacktracking = true;
        InitiateMovement(intendedMove);
        performingBacktracking = false;
    }
}
