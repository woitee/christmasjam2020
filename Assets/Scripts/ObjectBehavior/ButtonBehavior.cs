﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ButtonBehavior : MonoBehaviour {
    public GameColor color = GameColor.RED;

    private void Start() {
        Collider2D[] colliders = new Collider2D[10];
        GetComponent<BoxCollider2D>().OverlapCollider(new ContactFilter2D(), colliders);
        var collisions = colliders.Where(collider =>
            collider.gameObject != gameObject
            && collider.isTrigger == false
        ).ToList();

        if (collisions.Count % 2 == 1) LevelManager.Instance.ButtonPressed(color);
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        LevelManager.Instance.ButtonPressed(color);
    }

    private void OnTriggerExit2D(Collider2D collision) {
        LevelManager.Instance.ButtonUnpressed(color);
    }
}
