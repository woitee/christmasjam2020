﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class DoorBehavior : MonoBehaviour {
    [SerializeField]
    private bool _isClosed = false;
    public bool isClosed {
        get { return _isClosed; }
        set {
            _isClosed = value;
            spriteRenderer.sprite = isClosed ? closedSprite : openSprite;
            boxCollider.enabled = value;
        }
    }

    public GameColor color;

    public Sprite closedSprite;
    public Sprite openSprite;

    private SpriteRenderer spriteRenderer;

    BoxCollider2D boxCollider;

    void Start() {
        LevelManager.Instance.AddButtonChangeListener(OnButtonChange);
        spriteRenderer = GetComponent<SpriteRenderer>();
        boxCollider = GetComponent<BoxCollider2D>();
        boxCollider.enabled = isClosed;
    }

    private void OnValidate() {
        EditorApplication.delayCall += () => {
            GetComponent<SpriteRenderer>().sprite = isClosed ? closedSprite : openSprite;
        };
    }

    void OnButtonChange(GameColor color) {
        if (color == this.color) isClosed = !isClosed;
    }
}
