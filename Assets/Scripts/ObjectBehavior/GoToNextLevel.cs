﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GoToNextLevel : MonoBehaviour {
    private void OnTriggerEnter2D(Collider2D other) {
        LevelManager.Instance.GoToNextLevel();
    }
}
