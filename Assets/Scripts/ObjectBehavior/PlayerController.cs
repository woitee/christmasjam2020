﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerController : Movable {
    static Dictionary<KeyCode, Vector2Int> controlsToMovement = new Dictionary<KeyCode, Vector2Int> {
        { KeyCode.UpArrow, Vector2Int.up },
        { KeyCode.DownArrow, Vector2Int.down },
        { KeyCode.LeftArrow, Vector2Int.left},
        { KeyCode.RightArrow, Vector2Int.right },
        { KeyCode.W, Vector2Int.up },
        { KeyCode.S, Vector2Int.down },
        { KeyCode.A, Vector2Int.left },
        { KeyCode.D, Vector2Int.right }
    };

    // Update is called once per frame
    void Update() {
        CheckControls();
    }

    void CheckControls() {
        if (isMoving) return;

        foreach (var keyToVec in controlsToMovement) {
            if (Input.GetKey(keyToVec.Key)) {
                InitiateMovement(keyToVec.Value);
            }
        }
    }
}
