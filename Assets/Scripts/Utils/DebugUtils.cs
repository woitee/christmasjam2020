﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DebugUtils {
    public static bool isDebug = false;

    public static void Log(string message) {
        if (isDebug) Debug.Log(message);
    }
}
