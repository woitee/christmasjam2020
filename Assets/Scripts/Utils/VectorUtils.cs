﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class VectorUtils {
    public static bool ApproximatelyEquals(this Vector3 a, Vector3 b, float eps = 0.0001f) {
        return Mathf.Abs(a.x - b.x) < eps && Mathf.Abs(a.y - b.y) < eps;
    }
}
